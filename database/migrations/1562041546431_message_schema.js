'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MessageSchema extends Schema {
  up () {
    this.create('messages', (table) => {
      table.increments()
      table.integer('chat_room_id', 11).unsigned()
      table.integer('user_id', 11).unsigned()
      table.string('chat').notNullable()
      table
        .foreign('chat_room_id')
        .references('rooms.id')
        .onDelete('cascade')
      table
        .foreign('user_id')
        .references('users.id')
        .onDelete('cascade')
    })
  }

  down () {
    this.drop('messages')
  }
}

module.exports = MessageSchema
