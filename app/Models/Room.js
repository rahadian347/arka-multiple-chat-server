'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Room extends Model {
   
    messages() {
        return this.hasMany('App/Models/Message', 'id', 'chat_room_id',)
    }
    users() {
        return this.belongsToMany('App/Models/User').pivotTable('room_users')
    }
}

module.exports = Room
