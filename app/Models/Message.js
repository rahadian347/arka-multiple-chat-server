'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Message extends Model {
    user() {
        return this.belongsTo('App/Models/User', 'user_id', 'id')
    }
    room() {
        return this.belongsTo('App/Models/Room', 'chat_room_id', 'id')
    }
}

module.exports = Message
