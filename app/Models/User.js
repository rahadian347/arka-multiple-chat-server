'use strict'

const Model = use('Model')
const Hash = use('Hash')

class User extends Model {

  static get hidden() {
    return ['password']
  }

  static boot () {
    super.boot()

    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }
  
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  messages() {
    return this.hasMany('App/Models/Message', 'id', 'user_id')
  }
  rooms() {
    return this.belongsToMany('App/Models/Room').pivotTable('room_users')
  }


}

module.exports = User
