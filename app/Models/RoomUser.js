'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class RoomUser extends Model {
    
    messages() {
        return this.hasMany('App/Models/Message', 'id', 'chat_room_id')
    }
}

module.exports = RoomUser
