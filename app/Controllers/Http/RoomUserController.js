'use strict'

const Room = use('App/Models/Room')
const User = use('App/Models/User')

class RoomUserController {

  async index({ request, response }) {
    // const rooms = await Room.query()
    //   .with('users').with('messages')
    //   .fetch()
    const user = await User.find(11)
    const rooms = await user
      .rooms().with('users').with('messages')
      .wherePivot('user_id', 11)
      .fetch()

    response.status(200).json({
      message: 'Here are your rooms.',
      data: rooms

    })
  }

  async show({ request, response, params: { id } }) {
    const data = await User.query().where('id', id).with('rooms.messages.user').fetch()

    response.status(200).json({
      message: 'Here is your profile chatroom lists.',
      data: data
    })
  }
}

module.exports = RoomUserController
