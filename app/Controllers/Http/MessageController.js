'use strict'


const Message = use('App/Models/Message')
const Room = use('App/Models/Room')

class MessageController {

  async index({ request, response, view }) {

    try {
      const messages = await Message.query()
        .with('room')
        .fetch()

      response.status(200).json({
        message: 'Here are your messages.',
        data: messages

      })

    } catch (error) {
      response.status(400).json({
        message: error

      })
    }


  }

  async store({ request, response }) {
   try {
     const data = request.only(['chat_room_id', 'user_id', 'chat'])
     const message = await Message.create(data)

     return response.status(201).json({
       'message': 'success',
       'data': {
         'sender': message.user_id,
         'chat_room_id': message.chat_room_id,
         'chat': message.chat,
       }
     })
   } catch(error) {

   }
  }

  async destroy({ request, response, params: { id } }) {
    const message = await Message.find(id)
    await message.delete()

    response.status(200).json({
      message: 'Successfully deleted this message.',
      deleted: true
    })
  }


  async show({ params, request, response, }) {
  }

  /**
   * Render a form to update an existing message.
   * GET messages/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update message details.
   * PUT or PATCH messages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a message with id.
   * DELETE messages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
 
}

module.exports = MessageController
