'use strict'

const User = use('App/Models/User');

class UserController {

    async index({ response }) {
        const users = await User.query().with('messages.rooms').fetch()

        response.status(200).json({
            message: 'Here are your users.',
            data: users

        })
    }

    async show({ request, response, params: { id } }) {
        const data = await User.query().where('id',id).with('rooms.messages.user').fetch()

        response.status(200).json({
            message: 'Here is your project.',
            data: data
        })
    }


}

module.exports = UserController
