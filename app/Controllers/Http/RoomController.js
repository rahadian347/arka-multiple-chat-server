'use strict'

const Room = use('App/Models/Room');

/**
 * Resourceful controller for interacting with rooms
 */
class RoomController {


  async index({ request, response, view }) {
    const rooms = await Room.all()

    return response.status(200).json({
      message: 'Successfully retrieved rooms.',
      data: rooms
    })
  }

  async show({ request, response, params :{id} }) {
    const room = await Room.query().where('id', id).with('messages.user').fetch()

    return response.status(200).json({
      message: 'Found your room.',
      data: room
    })
  }


  async store({ request, response }) {
    const data = request.only(['name'])
    try {
      const roomExists = await Room.findBy('name', data)
      if (roomExists) {
        return response.status(400).send({ message: { error: 'Room already Exists' } })
      }
      const room = await Room.create(data)

      return response.status(201).json({
        'message': 'success',
        'data': {
          'name': room
        }
      })
    } catch (error) {
      return response.status(err.status).send(err)
    }
  }

  async update({ request, response }) {
    const { name, room } = request.post()

    room.name = name || room.name

    await room.save()

    response.status(200).json({
      message: 'Successfully updated this room.',
      data: room
    })
  }


  async delete({ request, response }) {
    const { room } = request.post()
    await room.delete()

    response.status(200).json({
      message: 'Successfully deleted this room.',
      deleted: true
    })
  }

}

module.exports = RoomController
