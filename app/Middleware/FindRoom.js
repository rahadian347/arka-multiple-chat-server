'use strict'
const Room = use('App/Models/Room')

class FindRoom {
    async handle({ request, response, params: { id } }, next) {
        // call next to advance the request
        const room = await Room.find(id)

        if (!room) {
            return response.status(404).json({
                message: 'Room not found.',
                id
            })
        }

        request.body.room = room

        await next()
    }
}

module.exports = FindRoom