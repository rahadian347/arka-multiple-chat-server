'use strict'


const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.post('/auth/register', 'AuthController.register')
Route.post('/auth/login', 'AuthController.login')


Route.group(() => {

  Route.get('/users', 'UserController.index').middleware(['auth'])
  Route.get('/users/:id', 'UserController.show').middleware(['auth', 'findUser'])

  Route.get('/rooms/', 'RoomController.index').middleware(['auth'])
  Route.get('/rooms/:id', 'RoomController.show').middleware(['auth', 'findRoom'])
  Route.post('/rooms/', 'RoomController.store').middleware(['auth'])
  Route.patch('/rooms/:id', 'RoomController.update').middleware(['auth', 'findRoom'])
  Route.delete('/rooms/:id', 'RoomController.delete').middleware(['auth', 'findRoom'])


  Route.get('/messages/', 'MessageController.index').middleware(['auth']).middleware(['auth'])
  Route.post('/messages/', 'MessageController.store').middleware(['auth']).middleware(['auth'])
  Route.post('/messages/', 'MessageController.destroy').middleware(['auth']).middleware(['auth'])

  Route.get('/chatrooms/', 'RoomUserController.index').middleware(['auth'])
  Route.get('/chatrooms/:id', 'RoomUserController.show').middleware(['auth'])

}).prefix('api/v1')


